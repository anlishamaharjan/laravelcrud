$(".alert").fadeOut(5000);

$('.table').DataTable();

function isValidEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function isInt(a) {
    if (Math.floor(a) == a && $.isNumeric(a)) {
        return true;
    } else {
        return false;
    }
}

$("#dob").datepicker({
    format: 'yyyy-mm-dd',
    startView: "year",
    autoclose: true
});


function hideErrSpan(outputClass) {
    $('.' + outputClass).hide();
}

$('body').on('click', 'span.error', function () {
    $(this).hide().prev('input').focus();
});
$('body').on('focus', 'input', function () {
    $(this).next('span.error').hide();
});
$('body').on('change', 'select', function () {
    $(this).next('span.error').hide();
});

function validate_create_form() {
    var err = 0;
    var name = $("#name").val();
    var email = $("#email").val();
    var address = $("#address").val();
    var phone = $("#phone").val();
    var dob = $("#dob").val();
    var nationality = $("#nationality").val();
    var contactmode = $("#contactmode").val();
    var gender = $('input[name=gender]:checked').val();
   
    function showErrSpan(outputClass, msg) {
        $('.' + outputClass).text(msg).show();
        err = 1;
    }
    if (name === '') {
        showErrSpan('name', '*Please enter your full name');
    }
    if (address === '') {
        showErrSpan('address', '*Please enter your address');
    }
    if(gender === '' || gender === undefined){
        showErrSpan('gender', '*Please enter your gender');
    }
    if (email === '') {
        showErrSpan('email', '*Please enter your email');
    } else if (!isValidEmail(email)) {
        showErrSpan('email', '*Invalid email');
    }
    if (phone === '') {
        showErrSpan('phone', '*Please enter your number');
    } else if (!isInt(phone)) {
        showErrSpan('phone', '*Invalid number');
    }
    if (dob === '') {
        showErrSpan('dob', '*Please enter your date of birth');
    }
    if (nationality === '') {
        showErrSpan('nationality', '*Please enter nationality');
    }
    if (contactmode === '') {
        showErrSpan('contactmode', '*Please enter preffered contact mode');
    }
    if (err === 1) {
        return false;
    } else {
        return true;
    }
}

