<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Validator;

class ClientController extends Controller 
{

    /**
     * Show the form for creating a new client.
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Show the list of clients.
     */
    public function index() 
    {
        $filepath = public_path() . '/csv/Data.csv';
        if (file_exists($filepath)) {
            $file = fopen($filepath, "r");
            while (!feof($file)) {
                $res[] = fgetcsv($file);
            }
            $client = $res;
            fclose($file);
        } else {
            $client = '';
        }
        Log::info('Showing client list');
        return view('index')->with('client', $client);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'name' => 'required|max:255',
                    'email' => 'required|email',
                    'gender' => 'required',
                    'address' => 'required',
                    'phone' => 'required',
                    'dob' => 'required',
                    'nationality' => 'required',
                    'contactmode' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('client/create')->withErrors($validator)->withInput();
            Log::debug('Unable to add client.Failed Validation');
        } else {
            $post = $_POST;
            unset($post['_token']);
            $filepath = public_path() . '/csv/Data.csv';
            if (file_exists($filepath)) {
                $handle = fopen("php://output", "w");
            } else {
                $handle = fopen("php://output", "a");
            }
            ob_start();
            fputcsv($handle, $post);
            $csvContent = ob_get_clean();
            file_put_contents($filepath, $csvContent, FILE_APPEND);
            Log::debug('New client added successfully.');
            return redirect('client')->with('message', 'Data saved in CSV File');
        }
    }

    /**
     * Show the client detail.
     */
    public function show($id)
    {
        $filepath = public_path() . '/csv/Data.csv';
        if (file_exists($filepath)) {
            $data = file($filepath);
            $client = explode(",", $data[$id]);
            Log::info('Showing client detail from csv line: '.$id);
        } else {
            $client = '';
        }
        return view('show')->with('client', $client);
    }

}
