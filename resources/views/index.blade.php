@extends('layout')
@section('content')
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('client') }}">Client List</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <a href="{{ URL::to('client/create') }}" class="btn btn-success">Add Client</a>    
            </form>
        </div>
    </div>
</nav>
@if(!empty($message))
<div class="alert alert-success">
    <p>{{$message}}</p>
</div>
@endif 

<div class="container">
    <div class="col-md-12">
        <div class="table-responsive">
            <table  cellpadding="0" cellspacing="0" width="100%" class="table table-striped">
                <thead>
                    <tr>
                        <th>S.No.</th>
                        <th>Full Name</th>
                        <th>Gender</th>
                        <th>E-mail</th>
                        <th>Address</th> 
                        <th>Phone</th> 
                        <th>DOB</th> 
                        <th>Nationality</th> 
                        <th>Contact Mode</th> 
                        <th></th> 
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $i = 1;
                    $key = 0;
                    ?>
                    @if(!empty($client))
                    @foreach ($client as $data)
                    @if($data[0])
                    <tr> 
                        <td>{{ $i++ }}</td>
                        <td>{{ ($data[0] != '') ? $data[0] : "N/A" }}</td>
                        <td>{{ ($data[1] != '') ? $data[1] : "N/A" }}</td>
                        <td>{{ ($data[2] != '') ? $data[2] : "N/A" }}</td>
                        <td>{{ ($data[3] != '') ? $data[3] : "N/A" }}</td>
                        <td>{{ ($data[4] != '') ? $data[4] : "N/A" }}</td>
                        <td>{{ ($data[5] != '') ? $data[5] : "N/A" }}</td>
                        <td>{{ ($data[6] != '') ? $data[6] : "N/A" }}</td>
                        <td>{{ ($data[7] != '') ? $data[7] : "N/A" }}</td>  
                        <td><a href="{{ URL::to('client/'.$key) }}" class="btn btn-primary">View Detail</a></td>
                        <?php $key++; ?>
                    </tr> 
                    @endif 
                    @endforeach  
                    @else
                    <tr>
                        <td colspan="9" align="center">No Clients</td>
                    </tr>
                    @endif                        
                </tbody>
            </table> 
        </div>
    </div>
</div>
@endsection
