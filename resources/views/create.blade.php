@extends('layout')
@section('content')
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ URL::to('client/create') }}">Add Client</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <a href="{{ URL::to('client') }}" class="btn btn-danger">Cancel</a>    
            </form>
        </div>
    </div>
</nav>
@if(!empty($errors->all()))
<div class="alert alert-danger">
    @foreach ($errors->all() as $message)
    <p>{{$message}}</p>
    @endforeach  
</div>
@endif    
{!! Form::open(['url'=>'client','name'=>'ClientForm','method'=>'post','id'=>'ClientForm','onsubmit'=>'return validate_create_form()']) !!}
<div class="container">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('Full Name', 'Full Name:') !!}
            {!! Form::text('name',null,['class'=>'form-control','id'=>'name']) !!}
            <span class="error name" style="display: none"></span>
        </div>
        <div class="form-group">
            {!! Form::label('Gender', 'Gender:') !!}
            Male{!! Form::radio('gender', 'male', null,['class'=>'gender-selection']) !!}
            Female{!! Form::radio('gender', 'female', null,['class'=>'gender-selection']) !!}
            <span class="error gender" style="display: none"></span>
        </div>
        <div class="form-group">
            {!! Form::label('Email', 'Email:') !!}
            {!! Form::text('email',null,['id'=>'email','class'=>'form-control']) !!}
            <span class="error email" style="display: none"></span>
        </div>
        <div class="form-group">
            {!! Form::label('Address', 'Address:') !!}
            {!! Form::text('address',null,['class'=>'form-control','id'=>'address']) !!}
            <span class="error address" style="display: none"></span>
        </div> 
        <div class="form-group">
            {!! Form::label('Phone', 'Phone:') !!}
            {!! Form::text('phone',null,['class'=>'form-control','id'=>'phone']) !!}
            <span class="error phone" style="display: none"></span>
        </div> 
        <div class="form-group">
            {!! Form::label('Date of Birth', 'Date of Birth:') !!}
            {!! Form::text('dob',null,['class'=>'form-control','id'=>'dob']) !!}
            <span class="error dob" style="display: none"></span>
        </div> 
        <div class="form-group">
            {!! Form::label('Nationality', 'Nationality:') !!}
            {!! Form::text('nationality',null,['class'=>'form-control','id'=>'nationality']) !!}
            <span class="error nationality" style="display: none"></span>
        </div> 
        <div class="form-group">
            {!! Form::label('Mode Of Contact', 'Mode Of Contact:') !!}
            {!! Form::select('contactmode', [''=>'Select an option','phone' => 'Phone','email' => 'Email','none' => 'None'], null,['class'=>'form-control','id'=>'contactmode']) !!}
            <span class="error contactmode" style="display: none"></span>
        </div>
        <div class="form-group">
            <button type = "submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</div>
{!! Form::close() !!}
@endsection
