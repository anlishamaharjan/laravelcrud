<html>
    <head>
        <title>Laravel CRUD</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="{{URL::asset('/bootstrap/bootstrap.min.css')}}" type="text/css"/>
        <link rel="stylesheet" href="{{URL::asset('/datepicker/datepicker3.css')}}" type="text/css" />
        <link rel="stylesheet" href="{{URL::asset('/datatables/datatables.min.css')}}" type="text/css" />
        <link rel="stylesheet" href="{{URL::asset('/css/main.css')}}" type="text/css" />
    </head>
    <body>
        @yield('content')
    </body>
    <script src="{{URL::asset('/js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('/bootstrap/bootstrap.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('/datepicker/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('/datatables/datatables.min.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('/js/script.js')}}" type="text/javascript"></script>
</html>
