@extends('layout')
@section('content')
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Client Detail</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <form class="navbar-form navbar-right">
                <a href="{{ URL::to('client') }}" class="btn btn-danger">Cancel</a>    
            </form>
        </div>
    </div>
</nav>
<div class="container">
    <div class="col-md-6">
        @if(!empty($client))
        <div class="form-group">
            <label>Full Name:</label>
            <span>{{$client[0]}}</span>
        </div>
        <div class="form-group">
            <label>Gender:</label>
            <span>{{$client[1]}}</span>
        </div>
        <div class="form-group">
            <label>E-mail:</label>
            <span>{{$client[2]}}</span>
        </div>
        <div class="form-group">
            <label>Address:</label>
            <span>{{$client[3]}}</span>
        </div>
        <div class="form-group">
            <label>Phone:</label>
            <span>{{$client[4]}}</span>
        </div>
        <div class="form-group">
            <label>Date Of Birth:</label>
            <span>{{$client[5]}}</span>
        </div>
        <div class="form-group">
            <label>Nationality:</label>
            <span>{{$client[6]}}</span>
        </div>
        <div class="form-group">
            <label>Contact Mode:</label>
            <span>{{$client[7]}}</span>
        </div>
        @else
        <span>No detail available</span>
        @endif   
    </div>
</div>


