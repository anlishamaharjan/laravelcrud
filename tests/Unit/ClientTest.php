<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClientTest extends TestCase 
{

    /**
     * Check out the /client index page
     */
    public function testClientIndex() 
    {
        $response = $this->call('GET', '/client');
        $this->assertEquals(200, $response->status());
    }

    /**
     * Check out the /client store method
     */
    public function testClientStore() 
    {
        $requested_arr = [
            'name' => 'Anlisha',
            'email' => 'test@gmail.com',
            'gender' => 'female',
            'address' => 'Kalanki',
            'phone' => '9849724350',
            'dob' => '1995-01-05',
            'nationality' => 'nepali',
            'contactmode' => 'phone'
        ];
        $response = $this->call('POST', 'client', $requested_arr);
        if ($response->getContent()) {
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }

    /**
     * @route client.create
     */
    public function testClientCreate()
    {
        $response = $this->call('GET', '/client/create');
        $this->assertEquals(200, $response->status());
    }

}
