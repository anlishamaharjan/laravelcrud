## About Project

A simple project that performs Create and Read part of Client CRUD application. Add Client form is created with necessary validation on form fields. Simple jquery validation is used in front-end and backend form validation is done via Laravel Validation class. Validated data are saved to a csv file. Client List displays the data fetched from the csv file.Datepicker plugin is used for selection of dates and datatableJS is used for pagination.Client detail is displayed reading the csv file.

## Testing

Laravel has built-in support for testing with PHPUnit. A new test case is created using command 'php artisan make:test ClientTest --unit' on the command line. phpunit.xml file tells PHPUnit to run the tests it finds in the ./tests/ directory.I wanted to test a route. Therefore, I added a method and used Laravel’s awesome testing methods to call the /client url.It only checks for a response status of 200. The function testClientStore() in ClientTest.php is used to test Client::store() method.ClientTest test functions are executed using command './vendor/bin/phpunit' on the command line.

## Logging

Information such as creation of new user, validation failure etc are written to the logs using laravel Log Facade.

##Deployment

Tried to deploy laravel application using Zeit static deployment which was in vain because the result was only the list of files shown in the deployed url. Using Dockerfile for Zeit deployment also caused a lot of issues because of difference in deployment environment and local environment for laravel. Now Desktop was used for the zeit which installed now cli and the application could be deployed using a simple 'now' command.
Heroku was another option I researched and tried to implement. Heroku cli was installed. Using command line 'heroku login' is necessary for authentication,then series of heroku command is followed to create app and push to heroku master.